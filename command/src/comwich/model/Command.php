<?php
namespace lbs\command\comwich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Command extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'commande';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = ['id' => 'string'];
    public $incrementing = false;



    public function items(){
        return $this->hasMany('\lbs\command\comwich\model\Item','command_id');
    }

    public function utilisateur(){
        return $this->belongsTo('\lbs\command\comwich\model\Utilisateur','client_id');
    }
    
}