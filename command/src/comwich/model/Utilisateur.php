<?php
namespace lbs\command\comwich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'utilisateur';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = ['id' => 'string'];
    public $incrementing = false;



    public function commandes(){
        return $this->hasMany('\lbs\command\comwich\model\Command', 'client_id');
    }
    
}