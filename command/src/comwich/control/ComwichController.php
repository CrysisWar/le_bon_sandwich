<?php

namespace lbs\command\comwich\control;


use Firebase\JWT\JWT;
use lbs\command\comwich\error\UnauthorizedError;
use lbs\command\comwich\model\Command;
use lbs\command\comwich\model\Item;
use lbs\command\comwich\model\Utilisateur;
use GuzzleHttp\Client;
use lbs\command\comwich\error\MissingDataException;
use lbs\command\comwich\error\NotFound;
use lbs\command\comwich\error\PhpError;
use lbs\command\comwich\error\BadRequest;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use lbs\command\comwich\utils\Calc;
use Ramsey\Uuid\Uuid;
use DateTime;


class ComwichController extends AbstractController {

    /*
     *
     * FONCTION POST COMMANDE
     *
     */
    public function postCommand(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        if(!isset($data['nom']) || !isset($data['mail']) || !isset($data['livraison']) || !isset($data['items']) )
        {
            return MissingDataException::MissingDataException($req,$resp);
        } else {
            $uuid4 = Uuid::uuid4();
            $token = random_bytes(32);
            $token = bin2hex($token);
            $command = new Command();
            $command->id = $uuid4;
            $command->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $command->mail = filter_var($data['mail'], FILTER_SANITIZE_EMAIL);
            $dateL['date'] = filter_var(preg_replace("([^0-9-])", "-", $data['livraison']['date']));
            $dateL['heure'] = filter_var(preg_replace("([^0-9:])", ":", $data['livraison']['heure']));
            $command->livraison = $dateL['date']." ".$dateL['heure'];
            $command->token = $token;
            $command->status = 1;
            $client = new Client(['base_uri' => 'http://api.catalogue.local/', 'timeout' => 2.0, ]);
            $forCommand = array();
            foreach($data['items'] as $row){
                try{
                    $urItem = filter_var($row['uri'], FILTER_SANITIZE_STRING);
                    $row['q'] = filter_var($row['q'], FILTER_SANITIZE_NUMBER_INT);
                    $response = $client->request('GET', $urItem);
                    $sand = (array)json_decode($response->getBody(), true);
                    array_push($forCommand,
                        array(
                            "uri" => $row['uri'],
                            "libelle" => $sand['sandwich']['nom'],
                            "tarif" => $sand['sandwich']['prix'],
                            "quantite" => $row['q']
                        ));
                } catch (\Exception $e) {
                    return PhpError::error($req,$resp);
                }
            }
            try {
                $command->save();
                $id = $command->id;
                foreach ($forCommand as $row) {
                    try{
                        $item = new Item();
                        $item->uri = $row['uri'];
                        $item->libelle = $row['libelle'];
                        $item->tarif = $row['tarif'];
                        $item->quantite = $row['quantite'];
                        $item->command_id = $id;
                        $item->save();
                    } catch (Exception $e) {
                        return PhpError::error($req,$resp);
                    }
                }
            } catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
            $data = [
                "type" => "success",
                "status" => 201,
                "method" => $req->getMethod(),
                "result" => [
                    "id" => $command->id,
                    "nom" => $command->nom,
                    "description" => $command->description,
                    "links" =>
                        [ "self" => [
                            "href" => $this->c['router']->pathFor('getCommand', ['id'=>"$id"])]
                        ]
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('postCommand', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }

    }

    /*
     *
     * FONCTION PUT COMMANDE
     *
     */
    public function updateCommand(Request $req, Response $resp, array $args): Response
    {
        //verifier status < 2
    }

    /*
     *
     * FONCTION PAYER COMMANDE
     *
     */
    public function payCommand(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $id = $args['id'];
        $command = Command::find($id);
        if(!empty($id)){
            if(isset($data['carte']) && isset($data['expiration']) && !isset($command->ref_paiement) ) {
                $command->mode_paiement = 11;
                    $date = new DateTime('NOW');
                $command->date_paiement = $date->format('Y-m-d H:i:s');
                $token = random_bytes(50);
                $token = bin2hex($token);
                $command->ref_paiement = $token;
                $command->status = 2;
                try{
                    $command->save();
                }catch(\Exception $e){
                    return PhpError::error($req,$resp);
                }
            } else {
                return MissingDataException::MissingDataException($req,$resp);
            }
        } else {
            return NotFound::error($req,$resp);
        }
        $data = [
            "commande" => [
                "nom" => $command->nom,
                "mail" => $command->mail,
                "id" => $command->id,
                "ref_paiement" => $command->ref_paiement,
                "montant" => Calc::calculMontant($req,$resp,$command->id)
            ]
        ];
        $resp = $resp->withStatus(201)
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withHeader('Location', $this->c['router']->pathFor('payCommand', ['id'=>"$id"]));
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }

    /*
     *
     * FONCTION GET COMMANDE
     *
     */
    public function getCommand(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id']; 
        $command = Command::select('id','created_at','updated_at','livraison','nom', 'mail', 'montant')->where('id','=', $id)->with('items')->get();
        if(!empty($command)){
            $command[0]['montant']= 0;
                foreach($command[0]['items'] as $row_item){
                    $command[0]['montant']+= $row_item['tarif'] * $row_item['quantite'];
                }
            $data = [
                'type' => 'resource',
                'links' => [
                    'self' => ['href' => $this->c['router']->pathFor('getCommand', ['id'=>"$id"])],
                    'command' => ['href' => '/command/'.$id.'/items/']
                ],
                'command' => $command,
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getCommand', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION GET FACTURE COMMANDE
     *
     */
    public function getPaidCommand(Request $req, Response $resp, array $args): Response
    {
        $ref_paiement = $req->getQueryParam('ref_paiement',NULL);
        $id = $args['id'];
        if(isset($ref_paiement))
        {
            $command = Command::where('id','=',$id)->where('ref_paiement','=',$ref_paiement)->first();
            if(!empty($command))
            {
                $data = [
                    'type' => 'resource',
                    'method' => 'GET',
                    'id_commande' => $command->id,
                    'nom' => $command->nom,
                    'mail' => $command->mail,
                    'ref_paiement' => $command->ref_paiement,
                    'montant' => Calc::calculMontant($req,$resp,$id),
                ];
                $resp = $resp->withStatus(201)
                    ->withHeader('Content-Type', 'application/json; charset=utf-8')
                    ->withHeader('Location', $this->c['router']->pathFor('getPaidCommand', ['id'=>"$id"]));
                $resp->getBody()->write(json_encode($data));
                return $resp;
            } else {
                return NotFound::error($req,$resp);
            }
        } else {
            return MissingDataException::MissingDataException($req,$resp);
        }
    }


    /*
     *
     * FONCTION AUTHENTICATION CLIENT
     *
     */
    public function postAuthClient(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $auth = $req->getHeader('Authorization');
        $auth = explode(" ", $auth[0]);
        $auth = base64_decode($auth[1]);
        $auth = explode(":", $auth);
        $user = Utilisateur::find($id);
        if(!empty($user)){
            if($user->mail===$auth[0] && password_verify($auth[1],$user->mdp)) {
                    $secret = "This_Sandwich_Is_Fuckin_Good";
                $token = JWT::encode([
                    'iss' => 'http://api.command.local:11080',
                    'aud' => 'http://api.command.local:11080',
                    'iat' => time(),
                    'exp'=> time()+3500,
                    'uid' => $user->id,
                    'lvl' => $user->level ],
                    $secret, 'HS512');
                $data = [
                    'jwt' => $token,
                ];
                $resp = $resp->withStatus(201)
                    ->withHeader('Content-Type', 'application/json; charset=utf-8')
                    ->withHeader('Location', $this->c['router']->pathFor('postAuthClient', ['id'=>"$id"]));
                $resp->getBody()->write(json_encode($data));
                return $resp;
            } else {
                return UnauthorizedError::error($req,$resp);
            }
        } else {
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION CREATION CLIENT
     *
     */
    public function postCreateClient(Request $req, Response $resp): Response
    {
        $data = $req->getParsedBody();
        if(!isset($data['nom'])&&!isset($data['prenom'])&&!isset($data['mail'])&&!isset($data['mdp'])) {
            $user = new Utilisateur();
                $uuid4 = Uuid::uuid4();
            $user->client_id = $uuid4;
            $user->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $user->prenom = filter_var($data['prenom'], FILTER_SANITIZE_STRING);
            $user->mail = filter_var($data['mail'], FILTER_SANITIZE_EMAIL);
            $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
            $user->level = 100;
            try{
                $user->save();
            } catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
            $resp = $resp->withStatus(200)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('postCreateClient'));
            return $resp;
        } else {
            return MissingDataException::MissingDataException($req,$resp);
        }
    }


    /*
     *
     * FONCTION GET CLIENT
     *
     */
    public function getClientProfile(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $user = Utilisateur::find($id);
        if(!empty($user)){
            $data = [
                'type' => $req->getMethod(),
                'client' => [
                    'nom' => $user->nom,
                    'prenom' => $user->prenom,
                    'mail' => $user->mail,
                    'cumul' => $user->cumul,
                ]
                ];
            try{
                $user->save();
            } catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getClientProfile', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        } else {
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION GET CLIENT COMMAND
     *
     */
    public function getClientCommand(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $user = Utilisateur::find($id)->with('commandes')->get();
        if(!empty($user)){
            $comm = array();
            foreach($user[0]['commandes'] as $row){
                array_push($comm,[
                    'commande' => $row['id'],
                    'nom' => $row['nom'],
                    'mail' => $row['mail'],
                    'montant' => calc::calculMontant($req,$resp, $row['id']),
                    'token' => $row['token'],
                    'ref_paiement' => $row['ref_paiement'],
                    'date_paiement' => $row['date_paiement'],
                    'status' => $row['status']
                ]);
            }
            $data = [
                'type' => $req->getMethod(),
                'historique' => $comm
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getClientCommand', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;

        } else {
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION POST CREATE CLIENT COMMAND
     *
     */
    public function postClientCommandCreate(Request $req, Response $resp, array $args): Response
    {
        $id = args['id'];
        $data = $req->getParsedBody();
        $user = Utilisateur::find($id);
        if (empty($user)){
            return NotFound::error($req,$resp);
        } else {
            $uuid4 = Uuid::uuid4();
            $token = random_bytes(32);
            $token = bin2hex($token);
            $command = new Command();
            $command->id = $uuid4;
            $command->nom = $user->nom;
            $command->mail = $user->mail;
            $command->client_id = $user->id;
            $dateL['date'] = filter_var(preg_replace("([^0-9-])", "-", $data['livraison']['date']));
            $dateL['heure'] = filter_var(preg_replace("([^0-9:])", ":", $data['livraison']['heure']));
            $command->livraison = $dateL['date'] . " " . $dateL['heure'];
            $command->status = 1;
            $command->token = $token;
            $client = new Client(['base_uri' => 'http://api.catalogue.local/', 'timeout' => 2.0,]);
            $forCommand = array();
            foreach ($data['items'] as $row) {
                try {
                    $urItem = filter_var($row['uri'], FILTER_SANITIZE_STRING);
                    $row['q'] = filter_var($row['q'], FILTER_SANITIZE_NUMBER_INT);
                    $response = $client->request('GET', $urItem);
                    $sand = (array)json_decode($response->getBody(), true);
                    array_push($forCommand,
                        array(
                            "uri" => $row['uri'],
                            "libelle" => $sand['sandwich']['nom'],
                            "tarif" => $sand['sandwich']['prix'],
                            "quantite" => $row['q']
                        ));
                } catch (\Exception $e) {
                    return PhpError::error($req, $resp);
                }
            }
            try {
                $command->save();
                $id = $command->id;
                foreach ($forCommand as $row) {
                    try {
                        $item = new Item();
                        $item->uri = $row['uri'];
                        $item->libelle = $row['libelle'];
                        $item->tarif = $row['tarif'];
                        $item->quantite = $row['quantite'];
                        $item->command_id = $id;
                        $item->save();
                    } catch (Exception $e) {
                        return PhpError::error($req, $resp);
                    }
                }
            } catch (\Exception $e) {
                return PhpError::error($req, $resp);
            }
            $data = [
                "commande" => [
                    "nom" => $command->nom,
                    "mail" => $command->mail,
                    "livraison" => [
                        "date" => $dateL['date'],
                        "heure" => $dateL['heure']
                    ],
                    "id" => $command->id,
                    "token" => $command->token,
                    "montant" => Calc::calculMontant($req,$resp,$command->id),
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('postClientCommandCreate'));
            $resp->getBody()->write(json_encode($data));
            return $resp;

        }
    }


    /*
     *
     * FONCTION POST PAYER CLIENT COMMAND
     *
     */
    public function postClientPayCommand(Request $req, Response $resp, array $args): Response
    {
        $user = $req->getAttributes();
        $id = $user['uid'];
        $command = Command::find($args['id']);
        if(!empty($command)){
            if(isset($data['carte']) && isset($data['expiration']) && !isset($command->ref_paiement) ) {
                $command->mode_paiement = 11;
                $date = new DateTime('NOW');
                $command->date_paiement = $date->format('Y-m-d H:i:s');
                $token = random_bytes(50);
                $token = bin2hex($token);
                $command->ref_paiement = $token;
                $command->status = 2;
                try{
                    $command->save();
                }catch(\Exception $e){
                    return PhpError::error($req,$resp);
                }
            } else {
                return MissingDataException::MissingDataException($req,$resp);
            }
        } else {
            return NotFound::error($req,$resp);
        }
        $data = [
            "commande" => [
                "nom" => $command->nom,
                "mail" => $command->mail,
                "id" => $command->id,
                "ref_paiement" => $command->ref_paiement,
                "montant" => Calc::calculMontant($req,$resp,$command->id)
            ]
        ];
        $resp = $resp->withStatus(201)
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withHeader('Location', $this->c['router']->pathFor('postClientPayCommand'));
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }
}