<?php
/**
 * Created by PhpStorm.
 * User: AdriWarch
 * Date: 05/03/2019
 * Time: 22:05
 */

namespace lbs\command\comwich\utils;

use lbs\command\comwich\model\Command;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class calc
{

    public static function calculMontant(Request $req, Response $resp, $id)
    {

        $command = Command::select('id','created_at','updated_at','livraison','nom', 'mail', 'montant')->where('id','=', $id)->with('items')->get();

        $command[0]['montant']= 0;
        foreach($command[0]['items'] as $row_item){
            $command[0]['montant']+= $row_item['tarif'] * $row_item['quantite'];
        }
        return $command[0]['montant'];
    }
}