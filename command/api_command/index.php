<?php

/* 
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu TD : Architecture et programmation d'applications web côté serveur/backend
Pauline Monteil : monteilpauline65@gmail.com
Michael Franiatte : michael.franiatte@gmail.com
Adrien Costa : crywarch@gmail.com
*/

/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: Le Bon Sandwich / api pour appli cliente
 * description: api pour la commande de sandwichs chez Le Bon Sandwich
 * type: SERVICE DE GESTION COMMANDES
 * @author: Canals, Monteil, Franiatte, Costa
 */

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;

use lbs\command\comwich\error\NotAllowed;


require __DIR__ . '/../src/vendor/autoload.php';
ini_set('display_errors', 1);

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$c = new \Slim\Container();      /* Create container */

$conf = [
    'settings' => [
        'displayErrorDetails'=>true,
        'dbconf'=>__DIR__ . '/../src/conf/config.ini'
    ],
    'notFoundHandler' => function ($c){
        return function ($rq,$rs){
            return NotAllowed::error($rs);
        };
    }
];

$app = new \Slim\App($conf);

/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: Le Bon Sandwich / api pour appli cliente
 * description: api pour la commande de sandwichs chez Le Bon Sandwich
 * type: SERVICE DE PRISE DE COMMANDES – mode non identifié
 * 
 * @author: Canals, Monteil, Costa, Franiatte
 */


$app->post('/commands[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->postCommand($req, $resp, $args);
})->setName('postCommand');

$app->put('/commands/{id}/', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->updateCommand($req, $resp, $args);
})->setName('updateCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckTokenMiddleware() );

$app->get('/commands/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->getCommand($req, $resp, $args);
})->setName('getCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckTokenMiddleware() );

$app->put('/commands/payer/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->payCommand($req, $resp, $args);
})->setName('payCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckTokenMiddleware() );

$app->get('/commands/facture/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->getPaidCommand($req, $resp, $args);
})->setName('getPaidCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckTokenMiddleware() );


// MODE AUTHENTIFIE

$app->get('/clients/create[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->postCreateClient($req, $resp, $args);
})->setName('postCreateClient');

$app->post('/clients/{id}/auth[/]', function(Request $req, Response $resp, $args){
   $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->postAuthClient($req,$resp,$args);
})->setName('postAuthClient');

$app->get('/clients/{id}/profil[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->getClientProfile($req,$resp,$args);
})->setName('getClientProfile')
    ->add(new \lbs\command\comwich\middlewares\CheckJWTMiddleware() );

$app->get('/clients/{id}/commands[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->getClientCommand($req,$resp,$args);
})->setName('getClientCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckJWTMiddleware() );

$app->post('/clients/{id}/create[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->postClientCommandCreate($req,$resp,$args);
})->setName('getClientCommandCreate')
    ->add(new \lbs\command\comwich\middlewares\CheckJWTMiddleware() );

$app->post('/clients/payer/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\command\comwich\control\ComwichController($this);
    return $c->postClientPayCommand($req,$resp,$args);
})->setName('postClientPayCommand')
    ->add(new \lbs\command\comwich\middlewares\CheckJWTMiddleware() )
    ->add(new \lbs\command\comwich\middlewares\CheckTokenMiddleware() );



$app->run();

/*
///http://api.command.local:11080/commands/e084872b-6328-46af-97fc-6cb909bf4f72
//http://api.command.local:8080/adminer (command_lbs)
*/
