<?php

namespace lbs\suivi\suivicom\middlewares;

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CheckTokenMiddleware
{

    public function __invoke(Request $request, Response $response, callable $next)
    {

        $token = $request->getQueryParam('token', null);
        if(is_null($token))
        {
            $token= $request->getHeader('X-lbs-token');
        }
        if(empty($token)){
            return \lbs\suivi\suivicom\utils\Writer::json_error_message($response,403,'Le token de la commande est absent ');
        }

        $id = $request->getAttribute('route')->getArgument('id');

        try{
            \lbs\suivi\suivicom\model\Command::where('id','=',$id)->where('token','=',$token)->firstOrFail();
        }
        catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return $response;
        };
        return $next($request,$response);
    }

}
