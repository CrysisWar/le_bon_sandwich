<?php
namespace lbs\suivi\suivicom\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Item extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];


    public function command() {
        return $this->belongsTo('\lbs\suivi\suivicom\model\Command', 'command_id');
    }
}