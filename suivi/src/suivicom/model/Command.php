<?php
namespace lbs\suivi\suivicom\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Command extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'commande';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts = ['id' => 'string'];
    public $incrementing = false;



    public function items(){
        return $this->hasMany('\lbs\suivi\suivicom\model\Item','command_id');
    }
    
}