<?php

namespace lbs\suivi\suivicom\control;

use lbs\suivi\suivicom\UnauthorizedError;
use lbs\suivi\suivicom\model\Command;
use lbs\suivi\suivicom\error\MissingDataException;
use lbs\suivi\suivicom\error\NotFound;
use lbs\suivi\suivicom\error\PhpError;
use lbs\suivi\suivicom\utils\Calc;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


class SuiviController extends AbstractController {

    /*
     *
     * FONCTION GET SUIVICOMMANDES
     *
     */
    public function getSuiviCommands(Request $req, Response $resp, array $args): Response
    {
        $page = $req->getQueryParam('page', 1);
        $size = $req->getQueryParam('size',5);
       $created_at = $req->getQueryParam('created_at');
        $status = $req->getQueryParam('status');
        $livraison = $req->getQueryParam('livraison');

        $commands = Command::select('id','created_at','updated_at','livraison','nom', 'mail', 'montant','status')->where('deleted_at','=',NULL);
        if(isset($created_at))
        {
            $commands->where('created_at','=', $created_at);
        }
        if(isset($status))
        {
            $commands->where('status','=', $status);
        }
        if(isset($status))
        {
            $commands->where('livraison','=', $livraison);
        }
        $commandsCounter = $commands->get();
        $count = count($commandsCounter);
        $pageMax = ceil($count/$size);
        if ($count >= $page*$size) {
            $pageCommands = $commands->skip(($page-1)*$size)->take($size)->get();
        }
        else {
            $pageCommands = $commands->skip(($pageMax-1)*$size)->take($count - $size*($pageMax-1))->get();
        }

        if ($page <= 0) $page = 1;
        if ($page > $pageMax) $page = $pageMax;

        $links['first'] = ['href' => $this->c['router']->pathFor('getSuiviCommands', []).'?page=1&size='.$size];
        $links['last'] = ['href' => $this->c['router']->pathFor('getSuiviCommands', []).'?page='.$pageMax.'&size='.$size];
        if ($page < $pageMax) $links['next'] = ['href' => $this->c['router']->pathFor('getSuiviCommands', []).'?page='.($page+1).'&size='.$size];
        if ($page > 1) $links['prev'] = ['href' => $this->c['router']->pathFor('getSuiviCommands', []).'?page='.($page-1).'&size='.$size];

        if(!$pageCommands->isEmpty()){
            $resp = $resp->withStatus(200)->withHeader('Content-Type', 'application/json');
            foreach ($pageCommands as $row)
            {
                $row['links'] = [ 'self' => [
                    'href' => $this->c['router']->pathFor('getSuiviCommands', ['id'=>$row['id']])
                ]];
                $row['montant'] = Calc::calculMontant($req,$resp,$row['id']);
            }
            $data = [
                'type' => 'collection',
                'count'=> $count,
                'locale' => 'fr-FR',
                'pagiation' => $links,
                'size' => count($pageCommands),
                'commands' => $pageCommands
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getSuiviCommands'));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION GET SUIVICOMMANDE
     *
     */
    public function getSuiviCommand(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id']; 
        $command = Command::select('id','created_at','updated_at','livraison','nom', 'mail', 'montant')->where('id','=', $id)->with('items')->get();
        if(!empty($command)){
            $command[0]['montant']= 0;
                foreach($command[0]['items'] as $row_item){
                    $command[0]['montant']+= $row_item['tarif'] * $row_item['quantite'];
                }
            $data = [
                'type' => 'resource',
                'links' => [
                    'self' => ['href' => $this->c['router']->pathFor('getSuiviCommand', ['id'=>"$id"])],
                ],
                'command' => $command,
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getSuiviCommand', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION UPDATE SUIVICOMMANDES
     *
     */
    public function updateSuiviCommand(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $id = $args['id'];
        $command= Command::find($id);
        if(!empty($command)){
            $command->status = filter_var($data['status'], FILTER_SANITIZE_NUMBER_INT);
            if($command->status > 5 || $command->status < 1)
            {
                try{
                    $command->save();
                }
                catch (\Exception $e) {
                    return PhpError::error($req,$resp);
                }
                $data = [
                    "type" => "success",
                    "method" => $req->getMethod(),
                    "result" => [
                        "id" => $command->id,
                        "status" => $command->status,
                    ]
                ];
                $resp = $resp->withStatus(201)
                    ->withHeader('Content-Type', 'application/json; charset=utf-8')
                    ->withHeader('Location', $this->c['router']->pathFor('updateSuiviCommand', ['id'=>"$id"]));
                $resp->getBody()->write(json_encode($data));
                return $resp;
            }
            else{
                return BadData::error($req,$resp);
            }
        }else{
            return NotFound::error($req,$resp);
        }
            
    }
    
}


