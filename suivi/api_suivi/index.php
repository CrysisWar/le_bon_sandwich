<?php

/* 
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu TD : Architecture et programmation d'applications web côté serveur/backend
Pauline Monteil : monteilpauline65@gmail.com
Adrien Costa : crywarch@gmail.com
Michael Franiatte : michael.franiatte@gmail.com
*/

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;
use \lbs\suivi\suivicom\control\SuiviController;
use \lbs\suivi\suivicom\error\NotAllowed;


require __DIR__ . '/../src/vendor/autoload.php';
ini_set('display_errors', 1);

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$c = new \Slim\Container();      /* Create container */

$conf = [
    'settings'=>[
        'displayErrorDetails' => true,
        'dbconf'=>__DIR__.'config.ini',
        'secret'=>'d787hdrtsrdfyh542446578gdgfhf',
        'determineRouteBeforeRouteMiddleware'=> true,
        'cors'=>[
            "methods"=> ["GET","POST","PUT","OPTIONS","DELETE"],
            "header.allow" =>['Content-Type','Authorization','X-lbs-token'],
            "header.expose"=>[],
            "max.age"=> 60+60,
            "credential"=> true
        ]

    ],
        'notFoundHandler' => function ($c){
        return function ($rq, $rs){
            return NotAllowed::error($rq, $rs);
            };
        }
];

$app = new \Slim\App($conf);

/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: Le Bon Sandwich / api pour appli cliente
 * description: api pour la commande de sandwichs chez Le Bon Sandwich
 * type: SERVICE DE PRISE DE COMMANDES – mode non identifié
 * 
 * @author: Canals, Monteil, Costa, Franiatte
 */

/**
 * 
 */


// This is the middleware
// It will add the Access-Control-Allow-Methods header to every request

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', 'http://api.suivi.local:12080')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});


/**
 * @api {get} /suivicommands[/]  Affiche toutes les commandes
 * @apiGroup Suivi
 * @apiName  getSuiviCommands
 * @apiVersion 1.0.1
 * @apiDescription Accès à une collection de type commandes :
 * permet d'accéder à la représentation de la collection commande.
 * Retourne une représentation json de la ressource, incluant son nom et
 * sa description.
 * @apiParam {Number} page Numero de page (optionnel)
 * @apiParam {Number} size Nombre de resultats par page (optionnel)
 * @apiExample Exemple de requête :
 *    GET suivicommands?size=10 HTTP/1.1
 *    Host: api.suivi.local
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {Number} count Compte du nombre de commandes totales
 * @apiSuccess (Succès : 200) {Number} size Compte du nombre de resultat par page
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apisuccess (Succès : 200) {Object} suivicommands Ressource commandes retournée
 * @apiSuccess (Succès : 200) {String} suivicommands.id Identifiant de la commandes
 * @apiSuccess (Succès : 200) {Sting} suivicommands.created_at date de création de la commande
 * @apiSuccess (Succès : 200) {String} suivicommands.updated_at date de mise à jour de la commande
 * @apiSuccess (Succès : 200) {String} suivicommands.livraison date de livraison de la commande
 * @apiSuccess (Succès : 200) {String} suivicommands.nom nom du client
 * @apiSuccess (Succès : 200) {String} suivicommands.mail mail du client
 * @apiSuccess (Succès : 200) {Number} suivicommands.montant montant de la commande
 * @apiSuccess (Succès : 200) {Number} suivicommands.status status de la commande
 * @apiSuccess (Succès : 200) {Object} suivicommands.links
 * @apisuccess (Succès : 200) {Link}   suivicommands.self.href lien vers 
 * @apiHeader (response headers) {String} Content-Type: format de représentation de la ressource réponse
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *          type": "collection",
 *          "count": 1502,
 *          "locale": "fr-FR",
 *       "pagiation": {
 *           "first": {
 *             "href": "/suivicommands/?page=1&size=5"
 *            },
 *          "last": {
 *              "href": "/suivicommands/?page=301&size=5"
 *           },
 *
 *          "next": {
 *               "href": "/suivicommands/?page=2&size=5"
 *           }
 *
 *     },
 *    "size": 5,
 *  "commands": [
 *  {
 *      "id": "e084872b-6328-46af-97fc-6cb909bf4f72",
 *       "created_at": "2018-10-21 17:37:00",
 *      "updated_at": "2019-02-26 16:47:20",
 *      "livraison": "2019-02-24 17:37:00",
 *      "nom": "Carpentier",
 *       "mail": "Carpentier@gmail.com",
 *      "montant": 23.5,
 *       "status": 2,
 *      "links": {
 *           "self": {
 *              "href": "/suivicommands/"
 *           }
 *      }
 *   },
 *   {
 *       "id": "195205ff-6beb-475c-806a-3652c7f17f92",
 *       "created_at": "2018-10-21 17:39:36",
 *     "updated_at": "2018-10-21 17:39:36",
 *      "livraison": "2018-10-22 08:57:14",
 *       "nom": "Turpin",
 *       "mail": "Turpin@dbmail.com",
 *       "montant": 11.25,
 *       "status": 1,
 *       "links": {
 *   "self": {
 *               "href": "/suivicommands/"
 *           }
 *       }
 *   }
 *}
 *
 *      
 * @apiError (Réponse : 404) NotFound Ressource non trouvée
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type": "error",
 *       "error" : 404,
 *       "message" : "Not Found"
 *     }
 */

$app->get('/suivicommands[/]', function(Request $req, Response $resp, $args){
    $c = new SuiviController($this);
    return $c->getSuiviCommands($req, $resp, $args);
})->setName('getSuiviCommands');
/**
 * @api {put} /suivicommands/{id}  modifier une commande
 * @apiGroup Suivi
 * @apiName updateSuiviCommand
 * @apiVersion 0.1.0
 *
 *
 * @apiDescription modification de la ressource Commande cible.
 * La commande doit exister, et est entièrement remplacée par celle transmise.
 * La requête doit obligatoirement fournir le status de la commande
 *
 *
 * @apiParam {Number} id identifiant de la commande ciblée
 * @apiParam  (request parameters) {String} status Status de la commande
 * @apiHeader (request headers) {String} Content-Type:=application/json format utilisé pour les données transmises
 *
 * @apiParamExample {request} exemple de paramètres
 *     {
 *          "status" : 1
 *     }
 *
 * @apiExample Exemple de requête :
 *    PUT /categories/8 HTTP/1.1
 *    Host: api.lbs.local
 *    Content-Type: application/json;charset=utf8
 *
 *    {
 *	     "status" : 2
 *     }
 *
 * @apiSuccess (Réponse : 200) {json} commande représentation json de la commande
 * @apiHeader (response headers) {String} Content-Type: format de représentation de la ressource réponse
 *
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     Content-Type: application/json;charset=utf8
 *
 *     {
 *       "type": "success",
 *       "method": "PUT",
 *       "result": {
 *                   "id": "e084872b-6328-46af-97fc-6cb909bf4f72",
 *                   "status": "2"
 *    }
*}
 *
 * @apiError (Réponse : 400) MissingParameter paramètre manquant dans la requête
 *
 * @apiErrorExample {json} exemple de réponse en cas d'erreur 400
 *     HTTP/1.1 400 Bad Request
 *     {
 *       "type": "error",
 *       "error" : 400,
 *       "message" : "donnée manquante (description)"
 *     }
 * @apiError (Réponse : 404) RessourceNotFound ressource catégorie inexistante
 *
 * @apiErrorExample {json} exemple de réponse en cas d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type": "error",
 *       "error" : 404,
 *       "message" : "ressource demandée inexistante : /categories/42/"
 *     }
 */
$app->put('/suivicommands/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\suivi\suivicom\control\SuiviController($this);
    return $c->updateSuiviCommand($req, $resp, $args);
})->setName('updateSuiviCommand');

$app->get('/suivicommands/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\suivi\suivicom\control\SuiviController($this);
    return $c->getSuiviCommand($req, $resp, $args);
})->setName('getSuiviCommand');

$app->map(['GET', 'POST', 'PUT'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});




$app->run();

/*
///http://api.suivi.local:12080/commands/e084872b-6328-46af-97fc-6cb909bf4f72
///http://api.command.local:11080/commands/?page=1
//http://api.command.local:11080/hello/michael
//http://api.command.local:8080/adminer (command_lbs)
*/
