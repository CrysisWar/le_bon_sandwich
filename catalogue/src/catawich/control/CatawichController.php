<?php

namespace lbs\catalogue\catawich\control;

use lbs\catalogue\catawich\error\MissingDataException;
use lbs\catalogue\catawich\model\Categorie;
use lbs\catalogue\catawich\model\Sandwich;
use lbs\catalogue\catawich\error\BadRequest;
use lbs\catalogue\catawich\error\NotFound;
use lbs\catalogue\catawich\error\PhpError;
use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class CatawichController extends AbstractController {

    /*
     *
     * FONCTION GET CATEGORIES
     *
     */
    public function getCategories(Request $req, Response $resp, array $args): Response
    {
        $page = $req->getQueryParam('page', 1);
        $size = $req->getQueryParam('size',5);
        $categories = Categorie::select('id','nom','description')->where('deleted_at','=',NULL);
        $categoriesCounter = $categories->get();
        $count = count($categoriesCounter);
        $pageMax = ceil($count/$size);
        if ($count >= $page*$size) {
            $pageCategories = $categories->skip(($page-1)*$size)->take($size)->get();
        }
        else {
            $pageCategories = $categories->skip(($pageMax-1)*$size)->take($count - $size*($pageMax-1))->get();
        }
        if ($page <= 0) $page = 1;
        if ($page > $pageMax) $page = $pageMax;
        $links['first'] = ['href' => $this->c['router']->pathFor('getCategories', []).'?page=1&size='.$size];
        $links['last'] = ['href' => $this->c['router']->pathFor('getCategories', []).'?page='.$pageMax.'&size='.$size];
        if ($page < $pageMax) $links['next'] = ['href' => $this->c['router']->pathFor('getCategories', []).'?page='.($page+1).'&size='.$size];
        if ($page > 1) $links['prev'] = ['href' => $this->c['router']->pathFor('getCategories', []).'?page='.($page-1).'&size='.$size];
        if(!$pageCategories->isEmpty()){
            foreach ($pageCategories as $categorie)
            {
                $categorie['links'] = [ 'self' => [
                    'href' => $this->c['router']->pathFor('getCategorie', ['id'=>$categorie['id']]).'sandwiches'
                ]];
            }
            $data = [
                'type' => 'collection',
                'count'=> $count,
                'size' => count($pageCategories),
                'locale' => 'fr-FR',
                'categories' => $pageCategories,
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getCategories'));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }


    /*
     *
     * FONCTION GET CATEGORIE
     *
     */
    public function getCategorie(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id']; 
        $categorie = Categorie::select('id','nom','description')->where('id','=',$id)->where('deleted_at','=',NULL)->get();
        if(!$categorie->isEmpty()){
            $data = [
                'type' => 'ressource',
                'locale' => 'fr-FR',
                'categorie' => $categorie,
                'links' => [
                    'self' => $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]),
                    'href' => $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]).'/'.'sandwiches',
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return PhpError::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION POST CATEGORIE
     *
     */
    public function creerCategorie(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        if(!isset($data['nom']) || !isset($data['description']))
        {
            return MissingDataException::MissingDataException($req,$resp);
        }
        else{
            $categorie = new Categorie();
            $categorie->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $categorie->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            try{
                $categorie->save();
                $id = $categorie->id;
            }
            catch (\Exception $e) {
                return  PhpError::error($req,$resp);
            }
            $data = [
                "type" => "success",
                "status" => 200,
                "method" => $req->getMethod(),
                "result" => [
                    "id" => $categorie->id,
                    "nom" => $categorie->nom,
                    "description" => $categorie->description,
                    "links" =>
                        [ "self" => [
                            "href" => $this->c['router']->pathFor('getCategorie', ['id'=>"$id"])
                            ]
                        ]
                ]
            ];
            $resp = $resp->withStatus(200)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }


    }

    /*
     *
     * FONCTION PUT CATEGORIE
     *
     */
    public function updateCategorie(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $id = $args['id'];
        $categorie = Categorie::find($id);
        if(!empty($categorie)){
            $categorie->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $categorie->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            try{
                $categorie->save();
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
        }
        else{
            $data = $req->getParsedBody();
            $id = $args['id'];
            $categorie = new Categorie();
            $categorie->id = $id;
            $categorie->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $categorie->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            try{
                $categorie->save();
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
        }
        $data = [
            "type" => "success",
            "status" => 201,
            "method" => $req->getMethod(),
            "result" => [
                "id" => $categorie->id,
                "nom" => $categorie->nom,
                "description" => $categorie->description,
                "links" =>
                    [ "self" => [
                        "href" => $this->c['router']->pathFor('getCategorie', ['id'=>"$id"])]
                    ]
            ]
        ];
        if ($req->getMethod()==='PUT') {
            $data['result']['links']['self']['href'] = $req->getUri()->getPath();
        }
        $resp = $resp->withStatus(201)
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withHeader('Location', $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]));
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }


    /*
     *
     * FONCTION DELETE CATEGORIE
     *
     */
    public function deleteCategorie(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $categorie = Categorie::find($id);
        if(!empty($categorie)){
            try {
                $categorie->delete();
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
            $data = [
                "type" => "success",
                "status" => 201,
                "method" => $req->getMethod(),
                "result" => [
                    "id" => $categorie->id,
                    "nom" => $categorie->nom,
                    "description" => $categorie->description,
                    "deleted_at" => $categorie->deleted_at,
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('deleteCategorie', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else {
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION POST SANDWICH
     *
     */
    public function postSandwich(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        if(!isset($data['nom']) || !isset($data['description']) || !isset($data['type_pain']) || !isset($data['img']) || !isset($data['prix']))
        {
            return BadRequest::error($req,$resp);
        }else {
            $sandwich= new Sandwich();
            $sandwich->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $sandwich->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $sandwich->type_pain = filter_var($data['type_pain'], FILTER_SANITIZE_STRING);
            $sandwich->img = filter_var($data['img'], FILTER_SANITIZE_STRING);
            $sandwich->prix = filter_var($data['prix'], FILTER_SANITIZE_STRING);
            try{
                $sandwich->save();
                $id = $sandwich->id;
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
        }
        $data = [
            "type" => "success",
            "status" => 201,
            "method" => $req->getMethod(),
            "result" => [
                "id" => $sandwich->id,
                "nom" => $sandwich->nom,
                "description" => $sandwich->description,
                "type_pain" => $sandwich->type_pain,
                "img" => $sandwich->img,
                "prix" => $sandwich->prix,
                "links" =>
                    [ "self" => [
                        "href" => $this->c['router']->pathFor('getSandwich', ['id'=>"$id"])]
                    ]
            ]
        ];
        $resp = $resp->withStatus(201)
            ->withHeader('Content-Type', 'application/json;  charset=utf-8')
            ->withHeader('Location', $this->c['router']->pathFor('getSandwich', ['id'=>"$id"]));
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }


    /*
     *
     * FONCTION PUT SANDWICH
     *
     */
    public function updateSandwich(Request $req, Response $resp, array $args): Response
    {
        $data = $req->getParsedBody();
        $id = $args['id'];
        $sandwich= Sandwich::find($id);
        if(!empty($sandwich)){
            $sandwich->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $sandwich->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $sandwich->type_pain = filter_var($data['type_pain'], FILTER_SANITIZE_STRING);
            $sandwich->img = filter_var($data['img'], FILTER_SANITIZE_STRING);
            $sandwich->prix = filter_var($data['prix'], FILTER_SANITIZE_STRING);
            try{
                $sandwich->save();
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
        }
        else{
            $data = $req->getParsedBody();
            $id = $args['id'];
            if(!isset($data['nom']) || !isset($data['description']) || !isset($data['type_pain']) || !isset($data['img']) || !isset($data['prix']))
            {
                return BadRequest::error($req,$resp);
            }else {
                $sandwich = new Sandwich();
                $sandwich->id = $id;
                $sandwich->nom = filter_var($data['nom'], FILTER_SANITIZE_STRING);
                $sandwich->description = filter_var($data['description'], FILTER_SANITIZE_STRING);
                $sandwich->type_pain = filter_var($data['type_pain'], FILTER_SANITIZE_STRING);
                $sandwich->img = filter_var($data['img'], FILTER_SANITIZE_STRING);
                $sandwich->prix = filter_var($data['prix'], FILTER_SANITIZE_STRING);
                try{
                    $sandwich->save();
                }
                catch (\Exception $e) {
                    return PhpError::error($req,$resp);
                }
            }
        }
        $data = [
            "type" => "success",
            "status" => 201,
            "method" => $req->getMethod(),
            "result" => [
                "id" => $sandwich->id,
                "nom" => $sandwich->nom,
                "description" => $sandwich->description,
                "type_pain" => $sandwich->type_pain,
                "img" => $sandwich->img,
                "prix" => $sandwich->prix,
                "links" =>
                    [ "self" => [
                        "href" => $this->c['router']->pathFor('getSandwich', ['id'=>"$id"])]
                    ]
            ]
        ];
        if ($req->getMethod()==='PUT') {
            $data['result']['links']['self']['href'] = $req->getUri()->getPath();
        }
        $resp = $resp->withStatus(201)
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->withHeader('Location', $this->c['router']->pathFor('getSandwich', ['id'=>"$id"]));
        $resp->getBody()->write(json_encode($data));
        return $resp;
    }


    /*
     *
     * FONCTION DELETE SANDWICH
     *
     */
    public function deleteSandwich(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $sandwich = Sandwich::find($id);
        if(!empty($sandwich)){
            try {
                $sandwich->delete();
            }
            catch (\Exception $e) {
                return PhpError::error($req,$resp);
            }
            $body = [
                "type" => "success",
                "status" => 201,
                "method" => $req->getMethod(),
                "uri" => $req->getUri()->getPath(),
                "result" => [
                    "id" => $sandwich->id,
                    "nom" => $sandwich->nom,
                    "description" => $sandwich->description,
                    "type_pain" => $sandwich->type_pain,
                    "img" => $sandwich->img,
                    "prix" => $sandwich->prix,
                    "deleted_at" => $sandwich->deleted_at
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('deleteSandwich', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($body));
            return $resp;
        }
        else {
            return NotFound::error($req,$resp);
        }
    }

    /*
     *
     * FONCTION GET SANDWICHES
     *
     */
    public function getSandwiches(Request $req, Response $resp, array $args): Response
    {
        $page = $req->getQueryParam('page', 1);
        $size = $req->getQueryParam('size',5);
        $pain = $req->getQueryParam('type_pain');
        $prix = $req->getQueryParam('prix');
        $sandwiches = Sandwich::select('id', 'nom', 'description','type_pain','img','prix')->where('deleted_at','=',NULL);
        if (isset($pain)){
            $sandwiches->where('type_pain','like','%'.$pain.'%');
        }
        if (isset($prix)){
            $sandwiches->where('prix','<=',$prix);
        }
        $sandwichesCounter = $sandwiches->get();
        $count = count($sandwichesCounter);
        $pageMax = ceil($count/$size);
        if ($count >= $page*$size) {
            $pageSandwiches = $sandwiches->skip(($page-1)*$size)->take($size)->get();
        }
        else {
            $pageSandwiches = $sandwiches->skip(($pageMax-1)*$size)->take($count - $size*($pageMax-1))->get();
        }
        if ($page <= 0) $page = 1;
        if ($page > $pageMax) $page = $pageMax;
        $links['first'] = ['href' => $this->c['router']->pathFor('getSandwiches', []).'?page=1&size='.$size];
        $links['last'] = ['href' => $this->c['router']->pathFor('getSandwiches', []).'?page='.$pageMax.'&size='.$size];
        if ($page < $pageMax) $links['next'] = ['href' => $this->c['router']->pathFor('getSandwiches', []).'?page='.($page+1).'&size='.$size];
        if ($page > 1) $links['prev'] = ['href' => $this->c['router']->pathFor('getSandwiches', []).'?page='.($page-1).'&size='.$size];
        if(!$pageSandwiches->isEmpty() && $count!=0){
            $data = [
                'type' => 'collection',
                'count'=> $count,
                'size' => count($pageSandwiches),
                'locale' => 'fr-FR',
                'pagiation' => $links,
                'sandwiches' => $pageSandwiches,
                'links' => [
                    'self' => [
                        'href' => $this->c['router']->pathFor('getSandwiches')
                    ]
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getSandwiches'));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }


    /*
     *
     * FONCTION GET SANDWICH
     *
     */
    public function getSandwich(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id']; 
        $sandwich = Sandwich::find($id);
        if(!empty($sandwich)){
            $resp = $resp->withStatus(200)->withHeader('Content-Type', 'application/json');
            $data = [
                'type' => 'ressource',
                'locale' => 'fr-FR',
                'sandwich' => $sandwich,
                'links' => [
                    'self' => ['href' => $this->c['router']->pathFor('getSandwich', ['id'=>"$id"])],
                    'categories' => ['href' => '/sandwiches/'.$id.'/categories/']
                ]
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getSandwich', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }


    /*
     *
     * FONCTION GET SANDWICHES-CATEGORIES
     *
     */
    public function getSandwichesCategorie(Request $req, Response $resp, array $args): Response
    {
        $id = $args['id'];
        $categorie = Categorie::where('id','=',$id)->with('sandwiches')->get()[0];
        if(!empty($categorie)){
            $sand = array();
            foreach($categorie['sandwiches'] as $row){
                array_push($sand,
                    array(
                        "id" =>$row['id'],
                        "nom" => $row['nom'],
                        "description" => $row['description'],
                        "type_pain" => $row['type_pain'],
                        "img" => $row['img'],
                        "prix" => $row['prix'],
                        'links' => [
                            'self' => ['href' => $this->c['router']->pathFor('getCategorie', ['id'=>"$id"]).'sandwiches/'],
                        ]
                    ));
            };
            $data = [
                'categorie' => [
                    'id' => $categorie->id, 'nom' => $categorie->nom, 'description' => $categorie->description],
                'sandwiches' => $sand,
            ];
            $resp = $resp->withStatus(201)
                ->withHeader('Content-Type', 'application/json; charset=utf-8')
                ->withHeader('Location', $this->c['router']->pathFor('getSandwichesCategorie', ['id'=>"$id"]));
            $resp->getBody()->write(json_encode($data));
            return $resp;
        }
        else{
            return NotFound::error($req,$resp);
        }
    }
}