<?php
namespace lbs\catalogue\catawich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Categorie extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    
    public function sandwiches() {
        return $this->belongsToMany('\lbs\catalogue\catawich\model\Sandwich', 'sand2cat', 'cat_id', 'sand_id');
        }
    
}