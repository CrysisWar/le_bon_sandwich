define({
  "name": "Catalogue",
  "version": "1.0.1",
  "description": "apiDoc de l'api Catalogue",
  "title": "Documentation api catalogue",
  "url": "http://api.catalogue.local:10080",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2019-03-11T05:00:45.883Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
