define({ "api": [
  {
    "type": "post",
    "url": "/categories/",
    "title": "créer une catégorie",
    "group": "Categories",
    "name": "creerCategorie",
    "version": "1.0.1",
    "description": "<p>Création d'une ressource de type Catégorie. La catégorie est ajoutée dans la base, son identifiant est créé. Le nom et la description de la catégorie doivent être fournis. La réponse retournée indique l'uri de la nouvelle ressource dans le header Location: et contient la représentation de la nouvelle ressource.</p>",
    "parameter": {
      "fields": {
        "request parameters": [
          {
            "group": "request parameters",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la nouvelle catégorie</p>"
          },
          {
            "group": "request parameters",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de paramètres",
          "content": "{\n  \"nom\"         : \"Vegan\",\n  \"description\" : \"sandwich sans viande\"\n}",
          "type": "request"
        }
      ]
    },
    "header": {
      "fields": {
        "request headers": [
          {
            "group": "request headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:",
            "defaultValue": "application/json",
            "description": "<p>format utilisé pour les données transmises</p>"
          }
        ],
        "response headers": [
          {
            "group": "response headers",
            "type": "String",
            "optional": false,
            "field": "Location:",
            "description": "<p>uri de la ressource créée</p>"
          },
          {
            "group": "response headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:application/json",
            "description": "<p>;charset=utf-8 format de représentation de la ressource réponse</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemple de requête :",
        "content": "POST /categories/ HTTP/1.1\nHost: api.catalogue.local\nContent-Type: application/json; charset=utf8\n{\n   \"nom\"         : \"Vegan\",\n   \"description\" : \"sandwich dans viande\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Succès : 201": [
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici success</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Object",
            "optional": false,
            "field": "categorie",
            "description": "<p>Ressource categorie créée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Number",
            "optional": false,
            "field": "categorie.id",
            "description": "<p>Identifiant de la catégorie créée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "categorie.nom",
            "description": "<p>Nom de la catégorie créée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "categorie.description",
            "description": "<p>Description de la catégorie créée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Object",
            "optional": false,
            "field": "links",
            "description": "<p>Liens vers les ressources associées à la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Link",
            "optional": false,
            "field": "links.self",
            "description": "<p>Lien vers la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Link",
            "optional": false,
            "field": "links.href",
            "description": "<p>lien vers les sandwichs de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 201 CREATED\nLocation: http://api.catalogue.local/categories/42\nContent-Type: application/json; charset=utf8\n{\n   \"type\": \"success\",\n   \"locale\": \"fr-FR\",\n   \"categorie\": [\n                     {\n                         \"id\": 25,\n                         \"nom\": \"Vegan\",\n                         \"description\": \"sandwich sans viande\"\n                     }\n                ],\n   \"links\": {\n              \"self\": \"/categories/25/\",\n              \"href\" : \"/categories/25/sandwiches/\"\n            }\n}",
          "type": "response"
        }
      ]
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Paramètre manquant dans la requête</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 422",
          "content": "HTTP/1.1 422 Missing Data Exception\n{\n  \"type\": \"error\",\n  \"error\" : 422,\n  \"message\" : \"Missing Data Exception\"\n}",
          "type": "json"
        },
        {
          "title": "Exemple d'erreur 500",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"type\": \"error\",\n  \"error\" : 500,\n  \"message\" : \"Internal Server Error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api_catalogue/index.php",
    "groupTitle": "Categories"
  },
  {
    "type": "delete",
    "url": "/categorie/{id}[/]",
    "title": "Supprime une catégorie",
    "group": "Categories",
    "name": "deleteCategorie",
    "version": "1.0.1",
    "description": "<p>Suppression de la ressource Catégorie cible (mode safe, ressource toujours présente dans la BDD). L'URI doit obligatoirement fournir un id de catégorie existant.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>identifiant de la catégorie ciblée</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemple de requête :",
        "content": "DELETE /categories/8 HTTP/1.1\nHost: api.catalogue.local",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Succès : 201": [
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici success</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>Status de la réponse, ici 201</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Methode de la réponse, ici PUT</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>Ressource retournée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Number",
            "optional": false,
            "field": "result.id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "result.nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "result.description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "result.delete_at",
            "description": "<p>Date de suppression de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 201 CREATED\nContent-Type: application/json;charset=utf8\n{\n   \"type\" : \"success\",\n   \"status\" : 201,\n   \"method\" : \"DELETE\",\n   \"result\" : {\n                 \"id\" : 42,\n                 \"nom\" : \"chauds\",\n                 \"description\" : \"Tous nos sandwichs chauds\",\n                 \"deleted_at\" : \"2019-03-11 05:30:20\"\n             }\n }",
          "type": "response"
        }
      ]
    },
    "header": {
      "fields": {
        "response headers": [
          {
            "group": "response headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:application/json",
            "description": "<p>;charset=utf8 format de représentation de la ressource réponse</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 404": [
          {
            "group": "Réponse : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Ressource non trouvée</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\": \"error\",\n  \"error\" : 404,\n  \"message\" : \"Not Found\"\n}",
          "type": "json"
        },
        {
          "title": "Exemple d'erreur 500",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"type\": \"error\",\n  \"error\" : 500,\n  \"message\" : \"Internal Server Error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api_catalogue/index.php",
    "groupTitle": "Categories"
  },
  {
    "type": "get",
    "url": "/categories/{id}",
    "title": "Consulter une catégorie",
    "group": "Categories",
    "name": "getCategories",
    "version": "1.0.1",
    "description": "<p>Accès à une ressource de type catégorie : permet d'accéder à la représentation de la ressource categorie désignée. Retourne une représentation json de la ressource, incluant son nom et sa description. Le résultat inclut un lien pour accéder à la liste des sandwichs de cette catégorie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant unique de la catégorie</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici ressource</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "categorie",
            "description": "<p>Ressource categorie retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "categorie.id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "categorie.nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "categorie.description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "links",
            "description": "<p>Liens vers les ressources associées à la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Link",
            "optional": false,
            "field": "links.self",
            "description": "<p>Lien vers la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Link",
            "optional": false,
            "field": "links.href",
            "description": "<p>lien vers les sandwichs de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 200 OK\n{\n   \"type\" : \"ressource,\n   \"locale\": \"fr-FR\"\n   \"categorie\" : {\n       \"id\"  : 4 ,\n       \"nom\" : \"chaud\",\n       \"description\" : \"sandwichs chauds : américain, burger\"\n   },\n   links : {\n       \"self\" : \"/categories/4/\n       \"href\" : \"/categories/4/sandwiches\" }\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Erreur : 404": [
          {
            "group": "Erreur : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Categorie inexistante</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\" : \"error',\n  \"error\" : 404,\n  \"message\" : Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api_catalogue/index.php",
    "groupTitle": "Categories"
  },
  {
    "type": "get",
    "url": "/categories[/]",
    "title": "Affiche toutes les catégories",
    "group": "Categories",
    "name": "getCategories",
    "version": "1.0.1",
    "description": "<p>Accès à une collection de type catégorie : permet d'accéder à la représentation de la collection categorie. Retourne une représentation json de la ressource, incluant son nom et sa description.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Numero de page (optionnel)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Nombre de resultats par page (optionnel)</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemple de requête :",
        "content": "GET /categories?page=3&?size=6 HTTP/1.1\nHost: api.catalogue.local",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Succès : 200": [
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici collection</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "count",
            "description": "<p>Compte du nombre de catégories totales</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Compte du nombre de resultat par page</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "locale",
            "description": "<p>Langage de la réponse, ici fr-FR</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "categories",
            "description": "<p>Ressource categorie retournée</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Number",
            "optional": false,
            "field": "categories.id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "categories.nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "String",
            "optional": false,
            "field": "categories.description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Object",
            "optional": false,
            "field": "categories.links",
            "description": "<p>Liens vers les ressources associées à la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Link",
            "optional": false,
            "field": "categories.links.self",
            "description": "<p>Lien vers la catégorie</p>"
          },
          {
            "group": "Succès : 200",
            "type": "Link",
            "optional": false,
            "field": "categories.links.self.href",
            "description": "<p>lien vers le sandwich de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": " HTTP/1.1 200 OK\n Content-Type: application/json;charset=utf8\n {\n    \"type\" : \"collection\",\n    \"count\" : 2,\n    \"size\" : 1,\n    \"locale\" : \"fr-FR\",\n    \"categories\" : [\n          {\n              \"id\"  : 42 ,\n              \"nom\" : \"cahuds\",\n              \"description\" : \"Tous nos sandwichs chauds\",\n              \"links\" : {\n                      \"self\" : {\n                          \"href\" : \"categories/42/sandwiches\"\n                       }\n                 }\n          }\n      ]\n}",
          "type": "response"
        }
      ]
    },
    "header": {
      "fields": {
        "response headers": [
          {
            "group": "response headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:",
            "description": "<p>format de représentation de la ressource réponse</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Réponse : 404": [
          {
            "group": "Réponse : 404",
            "optional": false,
            "field": "NotFound",
            "description": "<p>Ressource non trouvée</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 404",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"type\": \"error\",\n  \"error\" : 404,\n  \"message\" : \"Not Found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api_catalogue/index.php",
    "groupTitle": "Categories"
  },
  {
    "type": "put",
    "url": "/categories/{id}[/]",
    "title": "modifier une catégorie",
    "group": "Categories",
    "name": "updateCategorie",
    "version": "1.0.1",
    "description": "<p>modification de la ressource Catégorie cible. Création ou modification d'une catégorie. La requête doit obligatoirement fournir le nom et la description de la catégorie.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>identifiant de la catégorie ciblée</p>"
          }
        ],
        "request parameters": [
          {
            "group": "request parameters",
            "type": "String",
            "optional": false,
            "field": "nom",
            "description": "<p>Nom de la nouvelle catégorie</p>"
          },
          {
            "group": "request parameters",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>Description de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de paramètres",
          "content": "{\n  \"nom\" : \"chauds\",\n  \"description\" : \"Tous nos sandwichs chauds\"\n}",
          "type": "request"
        }
      ]
    },
    "header": {
      "fields": {
        "request headers": [
          {
            "group": "request headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:",
            "defaultValue": "application/json",
            "description": "<p>format utilisé pour les données transmises</p>"
          }
        ],
        "response headers": [
          {
            "group": "response headers",
            "type": "String",
            "optional": false,
            "field": "Content-Type:",
            "description": "<p>format de représentation de la ressource réponse</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Exemple de requête :",
        "content": "PUT /categories/8 HTTP/1.1\nHost: api.catalogue.local\nContent-Type: application/json;charset=utf8\n{\n   \"nom\" : \"chaud\",\n   \"description\" : \"Tous nos sandwichs chauds\"\n}",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Succès : 201": [
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Type de la réponse, ici success</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "method",
            "description": "<p>Methode de la réponse, ici PUT</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Object",
            "optional": false,
            "field": "result",
            "description": "<p>Ressource retournée</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Number",
            "optional": false,
            "field": "result.id",
            "description": "<p>Identifiant de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "result.nom",
            "description": "<p>Nom de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "String",
            "optional": false,
            "field": "result.description",
            "description": "<p>Description de la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Object",
            "optional": false,
            "field": "result.links",
            "description": "<p>Lien vers les ressources associées à la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Link",
            "optional": false,
            "field": "result.links.self",
            "description": "<p>Lien vers la catégorie</p>"
          },
          {
            "group": "Succès : 201",
            "type": "Link",
            "optional": false,
            "field": "result.links.self.href",
            "description": "<p>lien vers le sandwich de la catégorie</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "exemple de réponse en cas de succès",
          "content": "HTTP/1.1 201 CREATED\nContent-Type: application/json;charset=utf8\n{\n   \"type\" : \"success\",\n   \"method\" : \"PUT\",\n   \"result\" : {\n                 \"id\" : 42,\n                 \"nom\" : \"chauds\",\n                 \"description\" : \"Tous nos sandwichs chauds\"\n                 \"links\" : {\n                             \"self\" : {\n                                         \"href\" : \"/categories/42\"\n                                     }\n                             }\n             }\n }",
          "type": "response"
        }
      ]
    },
    "error": {
      "fields": {
        "Réponse : 422": [
          {
            "group": "Réponse : 422",
            "optional": false,
            "field": "MissingDataException",
            "description": "<p>Paramètre manquant dans la requête</p>"
          }
        ],
        "Réponse : 500": [
          {
            "group": "Réponse : 500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<p>Une erreur est survenue lors du traitement de la requete</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Exemple d'erreur 422",
          "content": "HTTP/1.1 422 Missing Data Exception\n{\n  \"type\": \"error\",\n  \"error\" : 422,\n  \"message\" : \"Missing Data Exception\"\n}",
          "type": "json"
        },
        {
          "title": "Exemple d'erreur 500",
          "content": "HTTP/1.1 500 Internal Server Error\n{\n  \"type\": \"error\",\n  \"error\" : 500,\n  \"message\" : \"Internal Server Error\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./api_catalogue/index.php",
    "groupTitle": "Categories"
  }
] });
