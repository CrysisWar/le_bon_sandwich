<?php

/* 
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu TD : Architecture et programmation d'applications web côté serveur/backend
Pauline Monteil : monteilpauline65@gmail.com
Michael Franiatte : michael.franiatte@gmail.com
Adrien Costa : crywarch@gmail.com
*/


/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: Le Bon Sandwich / api pour appli cliente
 * description: api pour la commande de sandwichs chez Le Bon Sandwich
 * type: SERVICE DE GESTION DU CATALOGUE
 * @author: Canals, Monteil, Franiatte, Costa
 */

// CONF

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;

require __DIR__ . '/../src/vendor/autoload.php';
ini_set('display_errors', 1);

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la connexion */

$c = new \Slim\Container();      /* Create container */

$conf = [
    'settings' => [
        'displayErrorDetails'=>true,
        'dbconf'=>__DIR__ . '/../src/conf/config.ini'
    ],
    'notFoundHandler' => function ($c){
        return function ($rq, $rs){
            return \lbs\catalogue\catawich\error\NotAllowed::error($rq, $rs);
        };
    }
];

$app = new \Slim\App($conf);


/**
 * @api {get} /categories/{id}  Consulter une catégorie
 * @apiGroup Categories
 * @apiName getCategories
 * @apiVersion 1.0.1
 * @apiDescription Accès à une ressource de type catégorie :
 * permet d'accéder à la représentation de la ressource categorie désignée.
 * Retourne une représentation json de la ressource, incluant son nom et
 * sa description.
 * Le résultat inclut un lien pour accéder à la liste des sandwichs de cette catégorie.
 * @apiParam {Number} id Identifiant unique de la catégorie
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici ressource
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apisuccess (Succès : 200) {Object} categorie Ressource categorie retournée
 * @apiSuccess (Succès : 200) {Number} categorie.id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} categorie.nom Nom de la catégorie
 * @apiSuccess (Succès : 200) {String} categorie.description Description de la catégorie
 * @apiSuccess (Succès : 200) {Object} links Liens vers les ressources associées à la catégorie
 * @apisuccess (Succès : 200) {Link}   links.self Lien vers la catégorie
 * @apisuccess (Succès : 200) {Link}   links.href lien vers les sandwichs de la catégorie
 * @apiSuccessExample {json} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     {
 *        "type" : "ressource,
 *        "locale": "fr-FR"
 *        "categorie" : {
 *            "id"  : 4 ,
 *            "nom" : "chaud",
 *            "description" : "sandwichs chauds : américain, burger"
 *        },
 *        links : {
 *            "self" : "/categories/4/
 *            "href" : "/categories/4/sandwiches" }
 *        }
 *     }
 * @apiError (Erreur : 404) NotFound Categorie inexistante
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type" : "error',
 *       "error" : 404,
 *       "message" : Not Found"
 *     }
 */
$app->get('/categories/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->getCategorie($req, $resp, $args);
})->setName('getCategorie');

/**
 * @api {post} /categories/  créer une catégorie
 * @apiGroup Categories
 * @apiName creerCategorie
 * @apiVersion 1.0.1
 * @apiDescription Création d'une ressource de type Catégorie.
 * La catégorie est ajoutée dans la base, son identifiant est créé.
 * Le nom et la description de la catégorie doivent être fournis.
 * La réponse retournée indique l'uri de la nouvelle ressource dans le header Location: et contient
 * la représentation de la nouvelle ressource.
 * @apiParam  (request parameters) {String} nom Nom de la nouvelle catégorie
 * @apiParam  (request parameters) {String} description Description de la catégorie
 * @apiHeader (request headers) {String} Content-Type:=application/json format utilisé pour les données transmises
 * @apiParamExample {request} exemple de paramètres
 *     {
 *       "nom"         : "Vegan",
 *       "description" : "sandwich sans viande"
 *     }
 * @apiExample Exemple de requête :
 *    POST /categories/ HTTP/1.1
 *    Host: api.catalogue.local
 *    Content-Type: application/json; charset=utf8
 *    {
 *       "nom"         : "Vegan",
 *       "description" : "sandwich dans viande"
 *    }
 * @apiSuccess (Succès : 201) {String} type Type de la réponse, ici success
 * @apiSuccess (Succès : 201) {String} locale Langage de la réponse, ici fr-FR
 * @apisuccess (Succès : 201) {Object} categorie Ressource categorie créée
 * @apiSuccess (Succès : 201) {Number} categorie.id Identifiant de la catégorie créée
 * @apiSuccess (Succès : 201) {String} categorie.nom Nom de la catégorie créée
 * @apiSuccess (Succès : 201) {String} categorie.description Description de la catégorie créée
 * @apiSuccess (Succès : 201) {Object} links Liens vers les ressources associées à la catégorie
 * @apisuccess (Succès : 201) {Link}   links.self Lien vers la catégorie
 * @apisuccess (Succès : 201) {Link}   links.href lien vers les sandwichs de la catégorie
 * @apiHeader (response headers) {String} Location: uri de la ressource créée
 * @apiHeader (response headers) {String} Content-Type:application/json;charset=utf-8 format de représentation de la ressource réponse
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 201 CREATED
 *     Location: http://api.catalogue.local/categories/42
 *     Content-Type: application/json; charset=utf8
 *     {
 *        "type": "success",
 *        "locale": "fr-FR",
 *        "categorie": [
 *                          {
 *                              "id": 25,
 *                              "nom": "Vegan",
 *                              "description": "sandwich sans viande"
 *                          }
 *                     ],
 *        "links": {
 *                   "self": "/categories/25/",
 *                   "href" : "/categories/25/sandwiches/"
 *                 }
 *     }
 * @apiError (Réponse : 422) MissingDataException Paramètre manquant dans la requête
 * @apiErrorExample {json} Exemple d'erreur 422
 *     HTTP/1.1 422 Missing Data Exception
 *     {
 *       "type": "error",
 *       "error" : 422,
 *       "message" : "Missing Data Exception"
 *     }
 * @apiError (Réponse : 500) InternalServerError Une erreur est survenue lors du traitement de la requete
 * @apiErrorExample {json} Exemple d'erreur 500
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "type": "error",
 *       "error" : 500,
 *       "message" : "Internal Server Error"
 *     }
 */
$app->post('/categories[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->creerCategorie($req, $resp, $args);
})->setName('creerCategorie');

/**
 * @api {put} /categories/{id}[/]  modifier une catégorie
 * @apiGroup Categories
 * @apiName updateCategorie
 * @apiVersion 1.0.1
 * @apiDescription modification de la ressource Catégorie cible.
 * Création ou modification d'une catégorie.
 * La requête doit obligatoirement fournir le nom et la description de la catégorie.
 * @apiParam {Number} id identifiant de la catégorie ciblée
 * @apiParam  (request parameters) {String} nom Nom de la nouvelle catégorie
 * @apiParam  (request parameters) {String} description Description de la catégorie
 * @apiHeader (request headers) {String} Content-Type:=application/json format utilisé pour les données transmises
 * @apiParamExample {request} exemple de paramètres
 *     {
 *       "nom" : "chauds",
 *       "description" : "Tous nos sandwichs chauds"
 *     }
 * @apiExample Exemple de requête :
 *    PUT /categories/8 HTTP/1.1
 *    Host: api.catalogue.local
 *    Content-Type: application/json;charset=utf8
 *    {
 *       "nom" : "chaud",
 *       "description" : "Tous nos sandwichs chauds"
 *    }
 * @apiSuccess (Succès : 201) {String} type Type de la réponse, ici success
 * @apiSuccess (Succès : 201) {String} method Methode de la réponse, ici PUT
 * @apisuccess (Succès : 201) {Object} result Ressource retournée
 * @apiSuccess (Succès : 201) {Number} result.id Identifiant de la catégorie
 * @apiSuccess (Succès : 201) {String} result.nom Nom de la catégorie
 * @apiSuccess (Succès : 201) {String} result.description Description de la catégorie
 * @apiSuccess (Succès : 201) {Object} result.links Lien vers les ressources associées à la catégorie
 * @apisuccess (Succès : 201) {Link}   result.links.self Lien vers la catégorie
 * @apisuccess (Succès : 201) {Link}   result.links.self.href lien vers le sandwich de la catégorie
 * @apiHeader (response headers) {String} Content-Type: format de représentation de la ressource réponse
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 201 CREATED
 *     Content-Type: application/json;charset=utf8
 *     {
 *        "type" : "success",
 *        "method" : "PUT",
 *        "result" : {
 *                      "id" : 42,
 *                      "nom" : "chauds",
 *                      "description" : "Tous nos sandwichs chauds"
 *                      "links" : {
 *                                  "self" : {
 *                                              "href" : "/categories/42"
 *                                          }
 *                                  }
 *                  }
 *      }
 * @apiError (Réponse : 422) MissingDataException Paramètre manquant dans la requête
 * @apiErrorExample {json} Exemple d'erreur 422
 *     HTTP/1.1 422 Missing Data Exception
 *     {
 *       "type": "error",
 *       "error" : 422,
 *       "message" : "Missing Data Exception"
 *     }
 * @apiError (Réponse : 500) InternalServerError Une erreur est survenue lors du traitement de la requete
 * @apiErrorExample {json} Exemple d'erreur 500
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "type": "error",
 *       "error" : 500,
 *       "message" : "Internal Server Error"
 *     }
 */
$app->put('/categories/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->updateCategorie($req, $resp, $args);
})->setName('updateCategorie');

/**
 * @api {delete} /categorie/{id}[/]  Supprime une catégorie
 * @apiGroup Categories
 * @apiName deleteCategorie
 * @apiVersion 1.0.1
 * @apiDescription Suppression de la ressource Catégorie cible (mode safe, ressource toujours présente dans la BDD).
 * L'URI doit obligatoirement fournir un id de catégorie existant.
 * @apiParam {Number} id identifiant de la catégorie ciblée
 * @apiExample Exemple de requête :
 *    DELETE /categories/8 HTTP/1.1
 *    Host: api.catalogue.local
 * @apiSuccess (Succès : 201) {String} type Type de la réponse, ici success
 * @apiSuccess (Succès : 201) {String} status Status de la réponse, ici 201
 * @apiSuccess (Succès : 201) {String} method Methode de la réponse, ici PUT
 * @apisuccess (Succès : 201) {Object} result Ressource retournée
 * @apiSuccess (Succès : 201) {Number} result.id Identifiant de la catégorie
 * @apiSuccess (Succès : 201) {String} result.nom Nom de la catégorie
 * @apiSuccess (Succès : 201) {String} result.description Description de la catégorie
 * @apiSuccess (Succès : 201) {String} result.delete_at Date de suppression de la catégorie
 * @apiHeader (response headers) {String} Content-Type:application/json;charset=utf8 format de représentation de la ressource réponse
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 201 CREATED
 *     Content-Type: application/json;charset=utf8
 *     {
 *        "type" : "success",
 *        "status" : 201,
 *        "method" : "DELETE",
 *        "result" : {
 *                      "id" : 42,
 *                      "nom" : "chauds",
 *                      "description" : "Tous nos sandwichs chauds",
 *                      "deleted_at" : "2019-03-11 05:30:20"
 *                  }
 *      }
 * @apiError (Réponse : 404) NotFound Ressource non trouvée
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type": "error",
 *       "error" : 404,
 *       "message" : "Not Found"
 *     }
 * @apiError (Réponse : 500) InternalServerError Une erreur est survenue lors du traitement de la requete
 * @apiErrorExample {json} Exemple d'erreur 500
 *     HTTP/1.1 500 Internal Server Error
 *     {
 *       "type": "error",
 *       "error" : 500,
 *       "message" : "Internal Server Error"
 *     }
 */
$app->delete('/categories/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->deleteCategorie($req, $resp, $args);
})->setName('deleteCategorie');

/**
 * @api {get} /categories[/]  Affiche toutes les catégories
 * @apiGroup Categories
 * @apiName  getCategories
 * @apiVersion 1.0.1
 * @apiDescription Accès à une collection de type catégorie :
 * permet d'accéder à la représentation de la collection categorie.
 * Retourne une représentation json de la ressource, incluant son nom et
 * sa description.
 * @apiParam {Number} page Numero de page (optionnel)
 * @apiParam {Number} size Nombre de resultats par page (optionnel)
 * @apiExample Exemple de requête :
 *    GET /categories?page=3&?size=6 HTTP/1.1
 *    Host: api.catalogue.local
 * @apiSuccess (Succès : 200) {String} type Type de la réponse, ici collection
 * @apiSuccess (Succès : 200) {Number} count Compte du nombre de catégories totales
 * @apiSuccess (Succès : 200) {Number} size Compte du nombre de resultat par page
 * @apiSuccess (Succès : 200) {String} locale Langage de la réponse, ici fr-FR
 * @apisuccess (Succès : 200) {Object} categories Ressource categorie retournée
 * @apiSuccess (Succès : 200) {Number} categories.id Identifiant de la catégorie
 * @apiSuccess (Succès : 200) {String} categories.nom Nom de la catégorie
 * @apiSuccess (Succès : 200) {String} categories.description Description de la catégorie
 * @apiSuccess (Succès : 200) {Object} categories.links Liens vers les ressources associées à la catégorie
 * @apisuccess (Succès : 200) {Link}   categories.links.self Lien vers la catégorie
 * @apisuccess (Succès : 200) {Link}   categories.links.self.href lien vers le sandwich de la catégorie
 * @apiHeader (response headers) {String} Content-Type: format de représentation de la ressource réponse
 * @apiSuccessExample {response} exemple de réponse en cas de succès
 *     HTTP/1.1 200 OK
 *     Content-Type: application/json;charset=utf8
 *     {
 *        "type" : "collection",
 *        "count" : 2,
 *        "size" : 1,
 *        "locale" : "fr-FR",
 *        "categories" : [
 *              {
 *                  "id"  : 42 ,
 *                  "nom" : "cahuds",
 *                  "description" : "Tous nos sandwichs chauds",
 *                  "links" : {
 *                          "self" : {
 *                              "href" : "categories/42/sandwiches"
 *                           }
 *                     }
 *              }
 *          ]
 *    }
 * @apiError (Réponse : 404) NotFound Ressource non trouvée
 * @apiErrorExample {json} Exemple d'erreur 404
 *     HTTP/1.1 404 Not Found
 *     {
 *       "type": "error",
 *       "error" : 404,
 *       "message" : "Not Found"
 *     }
 */
$app->get('/categories[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->getCategories($req, $resp, $args);
})->setName('getCategories');

$app->get('/sandwiches[/]',function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->getSandwiches($req, $resp, $args);
})->setName('getSandwiches');

$app->get('/sandwiches/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->getSandwich($req, $resp, $args);
})->setName('getSandwich');

$app->post('/sandwiches[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->postSandwich($req, $resp, $args);
})->setName('postSandwich');


$app->put('/sandwiches/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->updateSandwich($req, $resp, $args);
})->setName('updateSandwich');

$app->delete('/sandwiches/{id}[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->deleteSandwich($req, $resp, $args);
})->setName('deleteSandwich');


$app->get('/categories/{id}/sandwiches[/]', function(Request $req, Response $resp, $args){
    $c = new \lbs\catalogue\catawich\control\CatawichController($this);
    return $c->getSandwichesCategorie($req, $resp, $args);
})->setName('getSandwichesCategorie');

$app->run();
