-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `categorie` (`id`, `nom`, `description`, `deleted_at`) VALUES
(1,	'bio',	'sandwichs ingrédients bio et locaux',	NULL),
(2,	'végétarien',	'sandwichs végétariens - peuvent contenir des produits laitiers',	NULL),
(3,	'traditionnel',	'sandwichs traditionnels : jambon, pâté, poulet etc ..',	NULL),
(4,	'chaud',	'sandwichs chauds : américain, burger, ',	NULL),
(5,	'veggie',	'100% Veggie',	NULL),
(16,	'world',	'Tacos, nems, burritos, nos sandwichs du monde entier',	NULL),
(23,	'Vegan3',	'sandwich sans viande',	NULL);

DROP TABLE IF EXISTS `sand2cat`;
CREATE TABLE `sand2cat` (
  `sand_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sand2cat` (`sand_id`, `cat_id`) VALUES
(4,	3),
(4,	4),
(5,	3),
(5,	1),
(6,	4),
(6,	16);

DROP TABLE IF EXISTS `sandwich`;
CREATE TABLE `sandwich` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `type_pain` text NOT NULL,
  `img` text DEFAULT NULL,
  `prix` decimal(12,2) NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sandwich` (`id`, `nom`, `description`, `type_pain`, `img`, `prix`, `deleted_at`) VALUES
(4,	'le bucheron',	'un sandwich de bucheron : frites, fromage, saucisse, steack, lard grillé, mayo',	'baguette campagne',	NULL,	6.00,	NULL),
(5,	'jambon-beurre',	'le jambon-beurre traditionnel, avec des cornichons',	'baguette',	NULL,	5.25,	NULL),
(6,	'fajitas poulet',	'fajitas au poulet avec ses tortillas de mais, comme à Puebla',	'tortillas',	NULL,	6.50,	NULL),
(7,	'le forestier',	'un bon sandwich au gout de la forêt',	'pain complet',	NULL,	6.75,	NULL),
(8,	'corail',	'vegetarien',	'mie',	NULL,	3.00,	NULL);

DROP TABLE IF EXISTS `utilisateurgestion`;
CREATE TABLE `utilisateurgestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail` varchar(128) NOT NULL,
  `mdp` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `utilisateurgestion` (`id`, `mail`, `mdp`, `token`, `deleted_at`) VALUES
(2,	'test@test.fr',	'$2y$10$224pXVMr97smesbIWygbP.C758dEzQ5v.QqHOUqEeDgNJ.eM06CR.',	'ad34a9d77b620eb4730b6d2a5e9ba31823fb2ca761826c57d3094102acc1692c',	NULL);

-- 2019-03-15 05:32:25

