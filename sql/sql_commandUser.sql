DROP TABLE IF EXISTS `utilisateur`;

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `mail` varchar(256) NOT NULL,
  `mdp` varchar(128) NOT NULL,
  `cumul` decimal(8,2) NULL,
  `level` int(3) NOT NULL,
  `updated_at` datetime NULL,
  `deleted_at` datetime NULL,
  PRIMARY KEY (`id`)
)Engine=InnoDB charset=utf8;


INSERT INTO `utilisateur` (id,created_at,nom,prenom,mail,mdp,level) VALUES
('a38c9bad-5e9a-4683-b207-b74a99cda3b8','2019-03-05 17:10:50','Carpentier','Reitneprac','Carpentier@gmail.com','$2y$10$I//BQcp.KegG58gTRWW7o.ycSZxAu7skdND4c1zO1eTFcdQTKtxQK',100),
('819fd150-10a1-4071-a7c8-7aad597ceec0 ','2019-03-05 17:10:55','Turpin','Niprut','Turpin@dbmail.com','$2y$10$Qi0Vt5U8vyrjX50kUYCof.TD2GLOUen3yniUm9dxKW.MUe./rgkYq',100);
