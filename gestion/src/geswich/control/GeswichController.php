<?php

namespace lbs\gestion\geswich\control;

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Ramsey\Uuid\Uuid;
use Firebase\JWT\JWT;

class GeswichController extends AbstractController {

    /*
    public function listerSandwich()
    {
        $requete = \lbs\gestion\geswich\model\Sandwich::select();
        $lesSandwichs = $requete->get();
        return $lesSandwichs;
    }
    */

    public function afficherIndex(Request $request, Response $response, array $args): Response
    {
        return $this->c->view->render($response, 'index.html', [
        ]);
    }
    public function afficherCreerCompte(Request $request, Response $response, array $args): Response
    {
        return $this->c->view->render($response, 'creercomptetwig.html', [
        ]);
    }
    public function transfererLogin(Request $request, Response $response, array $args): Response
    {
        return $this->c->view->render($response, 'login.html', [
        ]);
    }
    public function afficherBienvenue(Request $request, Response $response, array $args): Response
    {
        $utilisateur_data = [];
        $utilisateur_data['mail'] = $_POST['email'];
        $utilisateur_data['mdp'] = $_POST['motdepasse'];
        if (isset($_POST['verifiermotdepasse'])){
            if ($_POST['verifiermotdepasse'] !== $utilisateur_data['mdp']){
                return $this->c->view->render($response, 'creercomptetwig.html', [
                    'name' => 'le mot de passe n\'est pas vérifié'
                ]);
            }
            else{
                $token = random_bytes(32);
                $token = bin2hex($token);
                $utilisateur = new \lbs\gestion\geswich\model\Utilisateur();
                $utilisateur->mail = $utilisateur_data['mail'];
                $motdepassehash = password_hash($utilisateur_data['mdp'], PASSWORD_DEFAULT);
                $utilisateur->mdp = $motdepassehash;
                $utilisateur->token = $token;
                $utilisateur->save();
                session_start ();
                $_SESSION['token'] = $token;
                return $this->c->view->render($response, 'bienvenuetwig.html', [
                ]);
            }
        }
        else{
            $utilisateur = \lbs\gestion\geswich\model\Utilisateur::where('mail', '=', $utilisateur_data['mail'])->firstOrFail();
            if (!empty($utilisateur)){
                if (password_verify($utilisateur_data['mdp'], $utilisateur['mdp'])){
                    $token = $utilisateur['token'];
                    session_start ();
                    $_SESSION['token'] = $token;
                    return $this->c->view->render($response, 'bienvenuetwig.html', [
                        ]);
                }
                else{
                    return $this->c->view->render($response, 'index.html', [
                        'name' => 'le mot de passe n\'est pas valide'
                    ]);
                }
            }
            else{
                return $this->c->view->render($response, 'index.html', [
                    'name' => 'le compte n\'existe pas'
                ]);
            }
        }
    }
    public function afficherSandwiches(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $sandwiches = \lbs\gestion\geswich\model\Sandwich::select('id','nom','description','type_pain','prix')->where('deleted_at','=',NULL)->get();
            return $this->c->view->render($response, 'sandwiches.html', [
                'sandwiches' => $sandwiches
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
    public function afficherCategories(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $categories = \lbs\gestion\geswich\model\Categorie::select('id','nom','description')->where('deleted_at','=',NULL)->get();
            return $this->c->view->render($response, 'categories.html', [
                'categories' => $categories
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
    public function afficherSandwiche(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $id = $args['id'];
            $sandwich = \lbs\gestion\geswich\model\Sandwich::find($id);
            return $this->c->view->render($response, 'sandwiche.html', [
                'name' => $sandwich
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
    public function afficherCategorie(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $id = $args['id'];
            $categorie = \lbs\gestion\geswich\model\Categorie::find($id);
            return $this->c->view->render($response, 'categorie.html', [
                'name' => $categorie
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
    public function updateSandwiche(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $data = $request->getParsedBody();
            $id = $args['id'];
            $sandwich = \lbs\gestion\geswich\model\Sandwich::find($id);
            $sandwich_data = [];
            $sandwich_data['nom'] = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $sandwich_data['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $sandwich_data['type_pain'] = filter_var($data['type_pain'], FILTER_SANITIZE_STRING);
            $sandwich_data['prix'] = filter_var($data['prix'], FILTER_SANITIZE_STRING);
            $sandwich = new \lbs\gestion\geswich\model\Sandwich();
            $sandwich->id = $id;
            $sandwich->nom = $sandwich_data['nom'];
            $sandwich->description = $sandwich_data['description'];
            $sandwich->type_pain = $sandwich_data['type_pain'];
            $sandwich->prix = $sandwich_data['prix'];
            $sandwich->save();
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
    public function updateCategorie(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $data = $request->getParsedBody();
            $id = $args['id'];
            $categorie = \lbs\gestion\geswich\model\Categorie::find($id);
            $categorie_data = [];
            $categorie_data['nom'] = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $categorie_data['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $categorie = new \lbs\gestion\geswich\model\Categorie();
            $categorie->id = $id;
            $categorie->nom = $categorie_data['nom'];
            $categorie->description = $categorie_data['description'];
            $categorie->save();
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }

    public function creerSandwiche(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $data = $request->getParsedBody();
            $sandwich_data = [];
            $sandwich_data['nom'] = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $sandwich_data['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $sandwich_data['type_pain'] = filter_var($data['type_pain'], FILTER_SANITIZE_STRING);
            $sandwich_data['prix'] = filter_var($data['prix'], FILTER_SANITIZE_STRING);
            $sandwich = new \lbs\gestion\geswich\model\Sandwich();
            $sandwich->nom = $sandwich_data['nom'];
            $sandwich->description = $sandwich_data['description'];
            $sandwich->type_pain = $sandwich_data['type_pain'];
            $sandwich->prix = empty($sandwich_data['prix'] ? 0.0 : $sandwich_data['prix']);
            $sandwich->save();
            return $this->c->view->render($response, 'creation.html', [
                'name' => 'sandwiche créé',
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }

    public function creerCategorie(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            $data = $request->getParsedBody();
            $categorie_data = [];
            $categorie_data['nom'] = filter_var($data['nom'], FILTER_SANITIZE_STRING);
            $categorie_data['description'] = filter_var($data['description'], FILTER_SANITIZE_STRING);
            $categorie = new \lbs\gestion\geswich\model\Categorie();
            $categorie->nom = $categorie_data['nom'];
            $categorie->description = $categorie_data['description'];
            $categorie->save();
            return $this->c->view->render($response, 'creation.html', [
                'name' => 'categorie créée',
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }

    public function afficherCreationSandwiche(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            return $this->c->view->render($response, 'creationsandwiche.html', [
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }

    public function afficherCreationCategorie(Request $request, Response $response, array $args): Response
    {
        session_start ();
        if (isset($_SESSION['token'])){
            return $this->c->view->render($response, 'creationcategorie.html', [
            ]);
        }
        else{
            return $this->c->view->render($response, 'login.html', [
            ]);
        }
    }
}
