<?php

namespace lbs\gestion\geswich\control;

class CatawichController extends \lbs\gestion\geswich\mf\control\AbstractController {
    
    public function __construct(){
        parent::__construct();
    }

    public function creerSandwich($nom, $description, $type_pain, $prix)
    {
        $sandwich = new \lbs\gestion\geswich\model\Sandwich();
        $sandwich->nom = $nom;
        $sandwich->description = $description;
        $sandwich->type_pain = $type_pain;
        $sandwich->prix = $prix;
        $sandwich->save();
        return 'ajout sandwiche';
    }

    public function updateSandwich($id, $nom, $description, $type_pain, $prix)
    {
        $sandwich = \lbs\gestion\geswich\model\Sandwich::find($id);
        if (!empty($sandwich)){
            $sandwich->id = $id;
            $sandwich->nom = $nom;
            $sandwich->description = $description;
            $sandwich->type_pain = $type_pain;
            $sandwich->prix = $prix;
            $sandwich->save();
            return 'mise à jour sandwiche';
        }
        else{
            return 'le sandwiche n\'existe pas';
        }
    }

    public function creerCategorie($nom, $description)
    {
        $categorie = new \lbs\gestion\geswich\model\Categorie();
        $categorie->nom = $nom;
        $categorie->description = $description;
        $categorie->save();
        return 'ajout categorie';
    }

    public function updateCategorie($id, $nom, $description)
    {
        $categorie = \lbs\gestion\geswich\model\Categorie::find($id);
        if (!empty($categorie)){
            $categorie->id = $id;
            $categorie->nom = $nom;
            $categorie->description = $description;
            $categorie->save();
            return 'mise à jour categorie';
        }
        else{
            return 'la categorie n\'existe pas';
        }
    }
}
