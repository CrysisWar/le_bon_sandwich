<?php
namespace lbs\gestion\geswich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Sandwich extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'sandwich';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function categories() {
        return $this->belongsToMany('\lbs\gestion\geswich\model\Categorie', 'sand2cat', 'sand_id', 'cat_id');
        }
}