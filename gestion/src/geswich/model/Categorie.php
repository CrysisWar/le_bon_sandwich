<?php
namespace lbs\gestion\geswich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Categorie extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'categorie';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
    
    public function sandwiches() {
        return $this->belongsToMany('\lbs\gestion\geswich\model\Sandwich', 'sand2cat', 'cat_id', 'sand_id');
        }
    
}