<?php
namespace lbs\gestion\geswich\model;
use \Illuminate\Database\Eloquent\SoftDeletes;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{
    use SoftDeletes;
    protected $table = 'utilisateurgestion';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $dates = ['deleted_at'];
}