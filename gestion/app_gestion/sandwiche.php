<?php

use Illuminate\Database\Capsule\Manager;

require __DIR__ . '/../src/vendor/autoload.php';

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var);
$db->setAsGlobal();           
$db->bootEloquent();  

session_start ();
if (isset($_SESSION['token'])){

    $sandwich = new lbs\gestion\geswich\control\CatawichController();

    if (!empty($_POST['id'])){
        $requete = $sandwich->updateSandwich($_POST['id'], $_POST['nom'], $_POST['description'], $_POST['type_pain'], $_POST['prix']);
    }
    else{
        $requete = $sandwich->creerSandwich($_POST['nom'], $_POST['description'], $_POST['type_pain'], $_POST['prix']);
    }

    echo $requete;
    echo '</br>';
    echo '<a href="/sandwiches">Retour</a>';

}
else {
    echo '<div>Veuillez vous connecter</div></br>';
    echo '<a href="/">Retour</a>';
}