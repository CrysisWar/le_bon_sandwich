<?php

/*
LP CIASIE : Dev. Applications & Services Web / Serveur
A l'intention de Gérôme Canals : gerome.canals@univ-lorraine.fr
Rendu TD : Architecture et programmation d'applications web côté serveur/backend
Pauline Monteil : monteilpauline65@gmail.com
Adrien Costa : crywarch@gmail.com
Michael Franiatte : michael.franiatte@gmail.com
*/


/**
 * File:  index.php
 * Creation Date: 17/10/2018
 * name: Le Bon Sandwich / api pour appli cliente
 * description: api pour la commande de sandwichs chez Le Bon Sandwich
 * type: APPLICATION DE GESTION DU CATALOGUE
 *
 * @author: Canals, Monteil, Costa, Franiatte
 */

//Configuration du projet

use\Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager;
use \lbs\gestion\geswich\control\GeswichController;
require __DIR__ . '/../src/vendor/autoload.php';

/**
* $loader = new Twig_Loader_Filesystem(__DIR__ . '/../src/templates');
* $twig = new Twig_Environment($loader, array('debug' => true));
* $tmpl = $twig->loadTemplate('test.twig');
* $tmpl->display( ['title' => 'Ah, quel BEAU TITRE', 'message' =>  'Oh, le beau Message'] ) ;
*/

$db = new Manager();
$var = parse_ini_file(__DIR__ . '/../src/conf/config.ini');

$db->addConnection($var);
$db->setAsGlobal();
$db->bootEloquent();

/*
$sandwich = new lbs\gestion\geswich\control\GeswichController();

echo 'méthode : listerSandwich';
echo "</br>";
$requete = $sandwich->listerSandwich();
echo $requete;
echo '</br>';
*/

$conf = [
    'settings' => [
        'displayErrorDetails' => true ,
        'tmpl_dir' => __DIR__ . '/../src/templates' ],
        'cors'=>[
            "methods"=> ["GET","POST","PUT","OPTIONS","DELETE"],
            "header.allow" =>['Content-Type','Authorization','X-lbs-token'],
            "header.expose"=>[],
            "max.age"=> 60+60,
            "credential"=> true
        ],
    'view' => function( $c ) {
        return new \Slim\Views\Twig(
            $c['settings']['tmpl_dir'],
            ['debug' => true,
            'cache' => false
            ]
        );
    }
];

// Create Slim app
$app = new \Slim\App(new \Slim\Container($conf));

// Define named route
/*
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $this->view->render($response, 'profile.html', [
        'name' => $args['name']
    ]);
})->setName('profile');
*/

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

$app->get('/', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->afficherIndex($request, $response, $args);
})->setName('afficherIndex');

$app->get('/creercompte', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->afficherCreerCompte($request, $response, $args);
})->setName('afficherCreerCompte');

$app->post('/login', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->transfererLogin($request, $response, $args);
})->setName('transfererLogin');

$app->post('/bienvenue', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->afficherBienvenue($request, $response, $args);
})->setName('afficherBienvenue');

$app->get('/sandwiches/{id}', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->afficherSandwiche($request, $response, $args);
})->setName('afficherSandwiche');

$app->get('/categories/{id}', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->afficherCategorie($request, $response, $args);
})->setName('afficherCategorie');

$app->get('/categories', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->afficherCategories($request, $response, $args);
})->setName('afficherCategories');

$app->get('/sandwiches', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->afficherSandwiches($request, $response, $args);
})->setName('afficherSandwiches');

$app->put('/sandwiche/{id}', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->updateSandwiche($request, $response, $args);
})->setName('updateSandwiche');

$app->put('/categorie/{id}', function ($request, $response, $args) {
    $c = new GeswichController($this);
    return $c->updateCategorie($request, $response, $args);
})->setName('updateCategorie');

$app->post('/creationsandwiche', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->creerSandwiche($request, $response, $args);
})->setName('creerSandwiche');

$app->post('/creationcategorie', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->creerCategorie($request, $response, $args);
})->setName('creerCategorie');

$app->get('/creationsandwiche', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->afficherCreationSandwiche($request, $response, $args);
})->setName('afficherCreationSandwiche');

$app->get('/creationcategorie', function ($request, $response, $args) {
    $c = new \lbs\gestion\geswich\control\GeswichController($this);
    return $c->afficherCreationCategorie($request, $response, $args);
})->setName('afficherCreationCategorie');

$app->map(['GET','PUT'], '/{routes:.+}', function($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});



// Run app
$app->run();
